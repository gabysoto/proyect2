package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.model.PersonComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
/**
 * 
 * @author Gabriel Soto Ramos
 *
 */
@Path("/person")
public class PersonManager {

	private static CircularSortedDoublyLinkedList<Person> list = new CircularSortedDoublyLinkedList<Person>(
			new PersonComparator());

	/**
	 * Gets all the people inside the list
	 * 
	 * @return : An array of all the people inside the list
	 */
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] getAllPeople() {
		return list.toArray(new Person[0]);
	}

	/**
	 * Gets a person in the list with the specified id
	 * 
	 * @param id : ID of the person 
	 * @return : The person, or a 404 Not Found if null
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPerson(@PathParam("id") long id) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getPersonId() == id)
				return Response.status(Response.Status.OK).entity(list.get(i)).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	/**
	 * Adds a person to the list
	 * 
	 * @param person : Person to add to the list
	 * @return : If successful, it returns an OK, else it returns a Not Modified error.
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPerson(Person person) {
		if (list.contains(person))
			return Response.notModified("That person is already in the list").build();
		list.add(person);
		return Response.status(201).build();
	}

	/**
	 * Deletes a person from the list
	 * 
	 * @param id : ID of the person to be deleted
	 * @return :  If successful, it returns an OK, else it returns a Not Modified error.
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deletePerson(@PathParam("id") long id) {
		for (int i = 0; i < list.size(); i++) {
			if (id == list.get(i).getPersonId()) {
				list.remove(list.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	/**
	 * Updates a person in the list
	 * This looks for a person with the id of the person given in the parameters
	 * then deletes such person from the list, and finally adds the new person 
	 * given in the parameters.
	 * 
	 * @param person Person to update
	 * @return  If successful, it returns an OK, else it returns a Not Modified error.
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePerson(Person person) {
		for (int i = 0; i < list.size(); i++) {
			if (person.getPersonId() == list.get(i).getPersonId()) {
				list.remove(i);
				list.add(person);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	/**
	 * Gets all the people with the same last name
	 * 
	 * @param lastName
	 * @return An array of all the people with the specified last name
	 */
	@GET
	@Path("/lastname/{lastName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] getPeopleByLastName(@PathParam("lastName") String lastName) {
		CircularSortedDoublyLinkedList<Person> l = new CircularSortedDoublyLinkedList<Person>(new PersonComparator());
		for(Person person : list) {
			if(person.getLastName().equals(lastName))
				l.add(person);
		}
		return l.toArray(new Person[0]);
	}


}
