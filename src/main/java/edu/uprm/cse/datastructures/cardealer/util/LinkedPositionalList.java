package edu.uprm.cse.datastructures.cardealer.util;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * 
 * @author unknown: This was taken from the iterator's lab.
 *
 * @param <E>
 */
public class LinkedPositionalList<E> implements PositionalList<E> {

	private static class DNode<E> implements Position<E> {
		private E element;
		private DNode<E> prev, next;

		public E getElement() {
			return element;
		}

		public DNode(DNode<E> prev, DNode<E> next, E element) {
			this.element = element;
			this.prev = prev;
			this.next = next;
		}

		@SuppressWarnings("unused")
		public DNode(E element) {
			this(null, null, element);
		}

		public DNode() {
			this(null, null, null);
		}

		public void setElement(E element) {
			this.element = element;
		}

		public DNode<E> getPrev() {
			return prev;
		}

		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}

		public DNode<E> getNext() {
			return next;
		}

		public void setNext(DNode<E> next) {
			this.next = next;
		}

		public void clean() {
			element = null;
			prev = next = null;
		}
	}

	private DNode<E> header, trailer;
	private int size;

	public LinkedPositionalList() {
		header = new DNode<>();
		trailer = new DNode<>();
		header.setNext(trailer);
		trailer.setPrev(header);
		size = 0;
	}

	private DNode<E> validate(Position<E> p) throws IllegalArgumentException {
		try {
			DNode<E> dp = (DNode<E>) p;
			if (dp.getPrev() == null || dp.getNext() == null)
				throw new IllegalArgumentException("Invalid internal node.");
			if (!contains(p))
				throw new IllegalArgumentException("List does not contain argument");
			return dp;
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Invalid position type.");
		}
	}

	private boolean contains(Position<E> p) {

		for (Position<E> element : positions()) {
			if (element.equals(p)) {
				return true;
			}
		}
		return false;
	}

	private Position<E> position(DNode<E> dn) {
		if (dn == header || dn == trailer)
			return null;
		return dn;
	}

	private DNode<E> addBetween(DNode<E> b, DNode<E> a, E e) {
		DNode<E> n = new DNode<>(b, a, e);
		b.setNext(n);
		a.setPrev(n);
		size++;
		return n;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public Position<E> first() {
		return position(header.getNext());
	}

	@Override
	public Position<E> last() {
		return position(trailer.getPrev());
	}

	@Override
	public Position<E> before(Position<E> p) throws IllegalArgumentException {
		return position(validate(p).getPrev());
	}

	@Override
	public Position<E> after(Position<E> p) throws IllegalArgumentException {
		return position(validate(p).getNext());
	}

	@Override
	public Position<E> addFirst(E e) {
		return addBetween(header, header.getNext(), e);
	}

	@Override
	public Position<E> addLast(E e) {
		return addBetween(trailer.getPrev(), trailer, e);
	}

	@Override
	public Position<E> addBefore(Position<E> p, E e) throws IllegalArgumentException {
		DNode<E> dp = validate(p);
		return addBetween(dp.getPrev(), dp, e);
	}

	@Override
	public Position<E> addAfter(Position<E> p, E e) throws IllegalArgumentException {
		DNode<E> dp = validate(p);
		return addBetween(dp, dp.getNext(), e);
	}

	@Override
	public E set(Position<E> p, E e) throws IllegalArgumentException {
		DNode<E> dp = validate(p);
		E etr = dp.getElement();
		dp.setElement(e);
		return etr;
	}

	@Override
	public E remove(Position<E> p) throws IllegalArgumentException {
		DNode<E> dp = validate(p);
		E etr = dp.getElement();
		DNode<E> b = dp.getPrev();
		DNode<E> a = dp.getNext();
		b.setNext(a);
		a.setPrev(b);
		dp.clean();
		size--;
		return etr;
	}

	@Override
	public Iterable<Position<E>> positions() {
		return new PositionIterable();
	}

	@Override
	public Iterator<E> iterator() {
		return new ElementIterator();
	}

	private class PositionIterator implements Iterator<Position<E>> {
		private DNode<E> cursor = header.getNext(), recent = null;

		@Override
		public boolean hasNext() {
			return cursor != trailer;
		}

		@Override
		public Position<E> next() throws NoSuchElementException {
			if (!hasNext())
				throw new NoSuchElementException("No more elements.");
			recent = cursor;
			cursor = cursor.getNext();
			return recent;
		}

		public void remove() throws IllegalStateException {
			if (recent == null)
				throw new IllegalStateException("remove() not valid at this state of the iterator.");
			DNode<E> b = recent.getPrev();
			DNode<E> a = recent.getNext();
			b.setNext(a);
			a.setPrev(b);
			recent.clean();
			recent = null;
			size--; // important because we are removing recent directly....
		}

	}

	private class ElementIterator implements Iterator<E> {
		Iterator<Position<E>> posIterator = new PositionIterator();

		@Override
		public boolean hasNext() {
			return posIterator.hasNext();
		}

		@Override
		public E next() throws NoSuchElementException {
			if (!hasNext())
				throw new NoSuchElementException("No more elements.");
			return posIterator.next().getElement();
		}

		public void remove() throws IllegalStateException {
			posIterator.remove();
		}
	}

	private class PositionIterable implements Iterable<Position<E>> {

		@Override
		public Iterator<Position<E>> iterator() {
			return new PositionIterator();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public E[] toArray() {
		E[] ar = (E[]) new Object[this.size];
		DNode<E> cnode = this.header.getNext();
		for (int i = 0; i < this.size; i++) {
			ar[i] = cnode.getElement();
			cnode = cnode.getNext();
		}
		return ar;
	}

//	@SuppressWarnings("unchecked")
//	@Override
//	public <T> T[] toArray(T[] t) {
//		t = (T[]) Array.newInstance(t.getClass().getComponentType(), this.size);
//		DNode<E> cnode = this.header.getNext();
//		for(int i=0; i<this.size; i++) {
//			t[i]=(T) cnode.getElement();
//			cnode=cnode.getNext();
//		}
//		return t;
//	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] array) {
		if (array.length < this.size()) {
			array = (T[]) Array.newInstance(array.getClass().getComponentType(), this.size());
		} else if (array.length > this.size()) {
			for (int i = this.size(); i < array.length; i++) {
				array[i] = null;
			}
		}
		DNode<E> node = (DNode<E>) this.header.getNext();
		for (int i = 0; i < this.size(); i++) {
			array[i] = (T) node.getElement();
			node = node.getNext();
		}
		return array;
	}

	@Override
	public boolean contains(E e) {
		DNode<E> cnode = this.header.getNext();
		while (cnode != trailer) {
			if (cnode.getElement() == e)
				return true;
			cnode = cnode.getNext();
		}
		return false;
	}
}
