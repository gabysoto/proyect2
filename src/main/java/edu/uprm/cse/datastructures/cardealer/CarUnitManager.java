package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.CarUnitComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/carunit")
public class CarUnitManager {

	private static CircularSortedDoublyLinkedList<CarUnit> list = new CircularSortedDoublyLinkedList<CarUnit>(
			new CarUnitComparator());

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public CarUnit[] getAllCarUnits() {
		return list.toArray(new CarUnit[0]);
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCarUnit(@PathParam("id") long id) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getCarUnitId() == id)
				return Response.status(Response.Status.OK).entity(list.get(i)).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCarUnit(CarUnit carUnit) {
		if (list.contains(carUnit))
			return Response.status(Response.Status.NOT_FOUND).build();
		list.add(carUnit);
		return Response.status(201).build();
	}

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCarUnit(@PathParam("id") long id) {
		for (int i = 0; i < list.size(); i++) {
			if (id == list.get(i).getCarUnitId()) {
				list.remove(list.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCarUnit(CarUnit carUnit) {
		for (int i = 0; i < list.size(); i++) {
			if (carUnit.getCarUnitId() == list.get(i).getCarUnitId()) {
				list.remove(i);
				list.add(carUnit);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
