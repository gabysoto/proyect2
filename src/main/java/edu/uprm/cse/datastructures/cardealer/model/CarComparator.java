package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car c1, Car c2) {
		return (c1.getCarBrand() + c1.getCarModel() + c1.getCarModelOption()+c1.getCarYear())
				.compareTo(c2.getCarBrand() + c2.getCarModel() + c2.getCarModelOption()+c2.getCarYear());
	}
}
