package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.util.LinkedPositionalList;
import edu.uprm.cse.datastructures.cardealer.util.Position;
import edu.uprm.cse.datastructures.cardealer.util.PositionalList;

/**
 * 
 * @author Gabriel Soto
 *
 */
@Path("/appointment")
public class AppointmentManager {
	@SuppressWarnings("unchecked")
	private static PositionalList<Appointment>[] list = new LinkedPositionalList[5];

	static {
		for (int i = 0; i < 5; i++) {
			list[i] = new LinkedPositionalList<Appointment>();
		}
	}

	/**
	 * index 
	 * 0 = monday 
	 * 1 = tuesday 
	 * 2 = wednesday 
	 * 3 = thursday 
	 * 4 = friday
	 **/

	/**
	 * Gets all appointments in the array of appointment lists by day.
	 * 
	 * @return array : array containing all appointment lists by day as arrays.
	 */
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[] getAllAppointments() {
		LinkedPositionalList<Appointment> ar = new LinkedPositionalList<Appointment>();
		for (Appointment p : list[0]) {
			ar.addLast(p);
		}
		for (Appointment p : list[1]) {
			ar.addLast(p);
		}
		for (Appointment p : list[2]) {
			ar.addLast(p);
		}
		for (Appointment p : list[3]) {
			ar.addLast(p);
		}
		for (Appointment p : list[4]) {
			ar.addLast(p);
		}
		return ar.toArray(new Appointment[0]);
	}

	/**
	 * Gets the appointment in the id given.
	 * 
	 * @param id : ID of appointment.
	 * @return if successful, it returns an OK, else it returns a Not Modified error.
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAppointment(@PathParam("id") int id) {
		for (int i = 0; i < list.length; i++) {
			for (Appointment p : list[i]) {
				if (p.getAppointmentId() == id)
					return Response.status(Response.Status.OK).entity(p).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	/**
	 * Adds the appointment to the list based on day given.
	 * 
	 * @param appointment : appointment to be added.
	 * @return if appointment could be added.
	 */
	@POST
	@Path("/add/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addAppointment(Appointment appointment, @PathParam("day") String day) {
		int d = dayIndex(day);
		if (d < 0 || d > 4)
			return Response.notModified("ERROR: " + day + " is not a valid weekday, try from 0 (monday) to 4 (friday).").build();
		if (contains(d,appointment.getAppointmentId())) {
			return Response.notModified("An appointment with the same id is already in the list").build();
		}
		list[d].addLast(appointment);
		return Response.status(201).build();
	}
	
	public boolean contains(int day, long id) {
		for (int i = 0; i < list.length; i++) {
			for (Appointment p : list[i]) {
				if (p.getAppointmentId() == id)
					return true;
			}
		}
		return false;
	}

	/**
	 * Updates the appointment of given id..
	 * 
	 * @param appointment : appointment with the updated information.
	 * @return if successful, it returns an OK, else it returns a Not Modified error.
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAppointment(Appointment appointment) {
		Iterable<Position<Appointment>> it;
		for (int i = 0; i < list.length; i++) {
			it = list[i].positions();
			for (Position<Appointment> p : it) {
				if (p.getElement().getAppointmentId() == appointment.getAppointmentId()) {
					list[i].addBefore(p, appointment);
					list[i].remove(p);
					return Response.status(Response.Status.OK).build();
				}
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	/**
	 * Deletes the appointment of given id.
	 * 
	 * @param id - id of appointment to be deleted.
	 * @return if successful, it returns an OK, else it returns a Not Modified error.
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteAppointment(@PathParam("id") long id) {
		Iterable<Position<Appointment>> it;
		for (int i = 0; i < list.length; i++) {
			it = list[i].positions();
			for (Position<Appointment> p : it) {
				if (p.getElement().getAppointmentId() == id) {
					list[i].remove(p);
					return Response.status(Response.Status.OK).build();
				}
			}
		}
		return Response.notModified("ERROR: ID: " + id + " not found").build();
	}

	/**
	 * Adds the appointment to the list based on day given.
	 * 
	 * @param day : gets the appointments of the specified day.
	 * @return an appointment array of the day specified.
	 */
	@GET
	@Path("/day/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[] getAppointmentByDay(@PathParam("day") String day) {
		int d = dayIndex(day);
		if (d < 0 || d > 4)
			return new Appointment[0];
		return list[d].toArray(new Appointment[0]);
	}
	
	/**
	 * Gets the index of a weekday
	 * 
	 * @param day 
	 * @return index of the specified weekday
	 */
	private int dayIndex(String day) {
		day = day.toLowerCase();
		int d = 0;
		try {
			d = Integer.parseInt(day);
		} catch (Exception e) {  //Generic exception, to prevent errors in different versions
			switch (day) {
			case "monday":
				return 0;
			case "tuesday":
				return 1;
			case "wednesday":
				return 2;
			case "thursday":
				return 3;
			case "friday":
				return 4;
			default:
				return -1;
			}
		}
		return d;
	}
}
