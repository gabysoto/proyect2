package edu.uprm.cse.datastructures.cardealer;

import java.util.Iterator;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private static CircularSortedDoublyLinkedList<Car> list = new CircularSortedDoublyLinkedList<Car>(
			new CarComparator());

	/**
	 * Gets all the cars from the list
	 * @return Car[] : The array of cars
	 */
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCars() {
		return list.toArray(new Car[0]);
	}

	/**
	 * Gets the first instance of a car with the specified id
	 * @param id : The id of the car
	 * @return Response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCar(@PathParam("id") int id) {
		Car car = list.get(getCarIndex(id));
		if (car == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.status(Response.Status.OK).entity(car).build();
	}

	/**
	 * Adds a car into the list
	 * @param car : Car to be inserted into the list
	 * @return Response : Returns a 201 if successfully added to the list
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		if (list.add(car))
			return Response.status(201).build();
		return Response.notModified("ERROR: Cannot add null car to the list").build();
	}

	/**
	 * Deletes a car with the specified id
	 * @param id : The id of the car that will be removed
	 * @return Response: Returns a 404 if not found
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") int id) {
		if (list.remove(getCarIndex(id)))
			return Response.status(Response.Status.OK).build();
		else
			return Response.status(Response.Status.NOT_FOUND).build();
	}

	/**
	 * Updates a car
	 * @param id : The id of the car that will be updated
	 * @param car : The car it should be updated with
	 * @return Response: Returns a 404 if not found
	 */
	@PUT
	@Path("/{id}/update")
	public Response updateCar(@PathParam("id") int id, Car car) {
		if (list.remove(getCarIndex(id))) {
			list.add(car);
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	/**
	 * Gets a car with the car id
	 * 
	 * @param carId : The id of the car
	 * @return Car
	 */
	public int getCarIndex(int carId) {
		Iterator<Car> i = list.iterator();
		int index = 0;
		while (i.hasNext() && i.next().getCarId() != carId) {
			index++;
		}
		return index;
	}
	/**
	 * Gets all the cars of the specific brand
	 * 
	 * @param brand : Brand of the cars
	 * @return
	 */
	@GET
	@Path("/brand/{brand}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCarsByBrand(@PathParam("brand") String brand) {
		CircularSortedDoublyLinkedList<Car> newCarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		for(Car car: list) {
			if(car.getCarBrand().equals(brand)) {
				newCarList.add(car);
			}
		}
		return newCarList.toArray(new Car[0]);
	}
	
	/**
	 * Gets all the cars of a specific year
	 * 
	 * @param year : Year of the cars
	 * @return
	 */
	@GET
	@Path("/year/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCarsByYear(@PathParam("year") int year) {
		CircularSortedDoublyLinkedList<Car> newCarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		for(Car car: list) {
			if(car.getCarYear() == year) {
				newCarList.add(car);
			}
		}
		return newCarList.toArray(new Car[0]);
	}

}
