package edu.uprm.cse.datastructures.cardealer.util;

import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Circular Sorted Doubly Linked List class that uses a comparator to sort
 * object automatically
 * 
 * @author Gabriel Soto Ramos
 *
 * @param <E>
 */
public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private DNode<E> header;
	private int currentSize = 0;
	private Comparator<E> comp;

	/**
	 * Constructor for the Circular Sorted Doubly Linked List without comparator
	 */
	public CircularSortedDoublyLinkedList() {
		this.header = new DNode<E>(null, null, null);
		this.header.setNext(header);
		this.header.setPrev(header);
	}

	/**
	 * Constructor for the Circular Sorted Doubly Linked List with comparator
	 */
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this();
		this.comp = comp;
	}

	/**
	 * Returns the iterator of the list
	 * @return Iterator
	 */
	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private DNode<E> cnode = header.getNext();

			@Override
			public boolean hasNext() {
				return cnode != header;
			}

			@Override
			public E next() {
				if (!this.hasNext())
					throw new NoSuchElementException();
				E result = cnode.getData();
				cnode = cnode.getNext();
				return result;
			}
		};
	}

	/**
	 * Adds an object in a position of the list according to the comparator
	 * 
	 * @param obj : Object to add to the list
	 * @return boolean : Will return false if the object is null
	 */

	@Override
	public boolean add(E obj) {
		if (obj == null)
			return false;
		DNode<E> node = new DNode<E>(obj, null, null);
		DNode<E> cnode = this.header.getNext();
		while (cnode != this.header && this.comp.compare(cnode.getData(), node.getData()) < 0) {
			cnode = cnode.getNext();
		}
		node.insertBefore(cnode);
		this.currentSize++;
		return true;
	}

	/**
	 * Returns the current size of the list as an integer
	 * 
	 * @return int
	 */
	@Override
	public int size() {
		return currentSize;
	}

	/**
	 * Removes the specified object from the list
	 * 
	 * @param obj : Object to remove
	 * @return boolean : Will return false if the object is not found
	 */
	@Override
	public boolean remove(E obj) {
		DNode<E> cnode = this.header.getNext();
		while (cnode != this.header) {
			if (cnode.getData().equals(obj)) {
				cnode.remove();
				this.currentSize--;
				return true;
			}
			cnode = cnode.getNext();
		}
		return false;
	}

	/**
	 * Removes an object in a specific position from the list
	 * 
	 * @param index : Position of the object in the list
	 * @return boolean : Will return false if the object is not found
	 */

	@Override
	public boolean remove(int index) {
		if (index >= this.size() || index < 0)
			return false;
		DNode<E> cnode = this.header.getNext();
		for (int i = 0; i < index; i++) {
			cnode = cnode.getNext();
		}
		this.currentSize--;
		cnode.remove();
		return true;
	}

	/**
	 * Removes all the instances of an object in the list
	 * 
	 * @param obj : Object to remove
	 * @return int : This returns the number of instances that were removed
	 */
	@Override
	public int removeAll(E obj) {
		DNode<E> cnode = this.header.getNext();
		int c = 0;
		while (cnode != this.header) {
			if (cnode.getData().equals(obj)) {
				DNode<E> temp = cnode.getNext();
				cnode.remove();
				this.currentSize--;
				c++;
				cnode = temp;
			} else {
				cnode = cnode.getNext();
			}
		}
		return c;
	}

	/**
	 * @return The first object of the list
	 */
	@Override
	public E first() {
		return this.header.getNext().getData();
	}

	/**
	 * @return The last object of the list
	 */
	@Override
	public E last() {
		return this.header.getPrev().getData();
	}

	/**
	 * Returns an object in a specific index from the list
	 * 
	 * @param index : The index of the object
	 * @return Object in the index specified or null if the index is invalid
	 */
	@Override
	public E get(int index) {
		if (this.isEmpty() || index < 0 || index >= this.size())
			return null;
		DNode<E> cnode = this.header.getNext();
		for (int i = 0; i < index; i++) {
			cnode = cnode.getNext();
		}
		return cnode.getData();

	}

	/**
	 * Returns a node in a specific index from the list
	 * 
	 * @param index : The index of the node
	 * @return The node in the index specified or null if the index is invalid
	 */
	public DNode<E> getNode(int index) {
		if (this.isEmpty() || index < 0 || index >= this.size())
			return null;

		DNode<E> cnode = this.header.getNext();
		for (int i = 0; i < index; i++) {
			cnode = cnode.getNext();
		}
		return cnode;
	}

	/**
	 * Removes every object from the list
	 */
	@Override
	public void clear() {
		while (this.remove(0))
			;
	}

	/**
	 * Checks if the list contains a specific object
	 * 
	 * @param e : The object/element to check for
	 * @return boolean
	 */
	@Override
	public boolean contains(E e) {
		if (e == null || isEmpty())
			return false;
		DNode<E> cnode = this.header.getNext();
		for (int i = 0; i < this.size(); i++) {
			if (cnode.getData().equals(e))
				return true;
			cnode = cnode.getNext();
		}
		return false;
	}

	/**
	 * Checks if the list is empty or not
	 * 
	 * @return boolean
	 */
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Return the index of the first instance of an object in the list
	 * 
	 * @param e : Object/element to check for
	 * @return int : Position/index of the first instance of the object
	 */
	@Override
	public int firstIndex(E e) {
		if (e == null || isEmpty())
			return -1;
		DNode<E> cnode = this.header.getNext();
		for (int i = 0; i < this.size(); i++) {
			if (cnode.getData().equals(e))
				return i;
			cnode = cnode.getNext();
		}
		return -1;
	}

	/**
	 * Return the index of the last instance of an object in the list
	 * 
	 * @param e : Object/element to check for
	 * @return int : Position/index of the last instance of the object
	 */
	@Override
	public int lastIndex(E e) {
		if (e == null || isEmpty())
			return -1;
		DNode<E> cnode = this.header.getNext();
		boolean found = false;
		int index = -1;
		for (int i = 0; i < this.size(); i++) {
			if (cnode.getData().equals(e)) {
				found = true;
				index = i;
			}
			if (cnode.getNext() == this.header)
				break;
			if (found && !cnode.getNext().getData().equals(e))
				break;
			cnode = cnode.getNext();
		}
		return index;
	}

	/**
	 * Converts the list into an array
	 * 
	 * @return The array of the objects in the list
	 */
	@SuppressWarnings("unchecked")
	public E[] toArray() {
		E[] ar = (E[]) new Object[this.size()];
		DNode<E> cnode = this.header.getNext();
		for (int i = 0; i < this.size(); i++) {
			ar[i] = cnode.getData();
			cnode = cnode.getNext();
		}
		return ar;
	}

	/**
	 * Converts the list into an array of a specified type
	 * 
	 * @param array : The type of the resulting array
	 * @return The array of the objects in the list
	 */
	@SuppressWarnings({ "unchecked", "hiding" })
	public <E> E[] toArray(E[] array) {
		if (array.length < this.size()) {
			array = (E[]) Array.newInstance(array.getClass().getComponentType(), this.size());
		} else if (array.length > this.size()) {
			for (int i = this.size(); i < array.length; i++) {
				array[i] = null;
			}
		}
		DNode<E> node = (DNode<E>) this.header.getNext();
		for (int i = 0; i < this.size(); i++) {
			array[i] = node.getData();
			node = node.getNext();
		}
		return array;
	}

	/**
	 * The node class which holds and links the data of the list
	 * 
	 * @author Gabriel Soto Ramos
	 * @param <E>
	 */
	@SuppressWarnings("hiding")
	public class DNode<E> {
		private DNode<E> next;
		private DNode<E> prev;
		private E data;

		public DNode(E data, DNode<E> next, DNode<E> prev) {
			this.data = data;
			this.prev = prev;
			this.next = next;
		}

		public DNode(E data) {
			this(data, null, null);
		}

		public DNode() {
			this(null, null, null);
		}

		public DNode<E> getNext() {
			return next;
		}

		public void setNext(DNode<E> next) {
			this.next = next;
		}

		public DNode<E> getPrev() {
			return prev;
		}

		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}

		public E getData() {
			return data;
		}

		public void setData(E data) {
			this.data = data;
		}

		public void insertBefore(DNode<E> node) {
			this.setNext(node);
			this.setPrev(node.getPrev());
			node.getPrev().setNext(this);
			node.setPrev(this);
		}

		public void insertAfter(DNode<E> node) {
			this.setPrev(node);
			this.setNext(node.getNext());
			node.getNext().setPrev(this);
			node.setNext(this);
		}
		
		public void remove() {
			this.getPrev().setNext(this.getNext());
			this.getNext().setPrev(this.getPrev());
			this.setData(null);
			this.setNext(null);
			this.setPrev(null);
		}

		public void swap(DNode<E> node) {
			this.setNext(node.getNext());
			node.setPrev(this.getPrev());
			this.setPrev(node);
			node.setNext(this);
		}
	}
}
