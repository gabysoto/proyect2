package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarUnitComparator implements Comparator<CarUnit> {

	@Override
	public int compare(CarUnit c0, CarUnit c1) {
		return c0.getVIN().compareTo(c1.getVIN());
	}

}
