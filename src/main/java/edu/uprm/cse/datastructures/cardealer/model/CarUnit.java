package edu.uprm.cse.datastructures.cardealer.model;

public class CarUnit {
	private long carUnitId; // internal id of the unit
	private long carId; // id of the car object that represents the general for the car. 
	private String VIN; // vehicle identification number
	private String color; // car color
	private String carPlate; // car plate (null until sold)
	private long personId; // id of the person who purchased the car. (null until purchased
	public CarUnit() {};
	public CarUnit(long carUnitId, long carId, String vIN, String color, String carPlate, long personId) {
		super();
		this.carUnitId = carUnitId;
		this.carId = carId;
		VIN = vIN;
		this.color = color;
		this.carPlate = carPlate;
		this.personId = personId;
	}
	public long getCarUnitId() {
		return carUnitId;
	}
	public void setCarUnitId(long carUnitId) {
		this.carUnitId = carUnitId;
	}
	public long getCarId() {
		return carId;
	}
	public void setCarId(long carId) {
		this.carId = carId;
	}
	public String getVIN() {
		return VIN;
	}
	public void setVIN(String vIN) {
		VIN = vIN;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getCarPlate() {
		return carPlate;
	}
	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((VIN == null) ? 0 : VIN.hashCode());
		result = prime * result + (int) (carId ^ (carId >>> 32));
		result = prime * result + ((carPlate == null) ? 0 : carPlate.hashCode());
		result = prime * result + (int) (carUnitId ^ (carUnitId >>> 32));
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + (int) (personId ^ (personId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarUnit other = (CarUnit) obj;
		if (VIN == null) {
			if (other.VIN != null)
				return false;
		} else if (!VIN.equals(other.VIN))
			return false;
		if (carId != other.carId)
			return false;
		if (carPlate == null) {
			if (other.carPlate != null)
				return false;
		} else if (!carPlate.equals(other.carPlate))
			return false;
		if (carUnitId != other.carUnitId)
			return false;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (personId != other.personId)
			return false;
		return true;
	}
	
	
}
